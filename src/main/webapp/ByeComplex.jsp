<%-- 
    Document   : ByeComplex
    Created on : Apr 11, 2014, 12:15:11 AM
    Author     : rossbille
--%>
<%@ taglib prefix="s" uri="/struts-tags" %> 

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ByeComplex</title>
    </head>
    <body>
        <h1>Bye ${complexObject.name}!</h1>
		
		<h1>Bye <s:property value="%{complexObject.name}"/>!</h1>
		
		<s:set name="age" value="complexObject.age"/>
		<p>You are too young at ${complexObject.age} year<s:if test="%{#age!=1}">s</s:if> old</p>
		
		<p>You are too young at <s:property value="%{complexObject.age}"/> year<s:if test="%{complexObject.age!=1}">s</s:if> old</p>
		
		See <a href="http://stackoverflow.com/questions/8007858/whats-the-difference-between-and-signs-in-struts-tags">http://stackoverflow.com/questions/8007858/whats-the-difference-between-and-signs-in-struts-tags</a> for more info
    </body>
</html>
