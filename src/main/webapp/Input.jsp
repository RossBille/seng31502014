<%-- 
    Document   : Input
    Created on : Apr 10, 2014, 11:49:06 PM
    Author     : rossbille
--%>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Input Page</title>
    </head>
    <body>
        <h1>Input</h1>
		<s:form action="Parameters">
			<s:textfield name="name"/>
		</s:form>
    </body>
</html>
