<%-- 
    Document   : ComplexInput
    Created on : Apr 11, 2014, 12:06:45 AM
    Author     : rossbille
--%>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Complex Objects</title>
    </head>
    <body>
        <h1>Input</h1>
		<s:form action="Parameters2">
			<s:textfield name="complexObject.name" placeholder="name"/>
			<s:textfield name="complexObject.age" placeholder="age"/>
			<s:submit/>
		</s:form>
    </body>
</html>
