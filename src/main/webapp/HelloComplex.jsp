<%-- 
    Document   : HelloComplex
    Created on : Apr 11, 2014, 12:13:28 AM
    Author     : rossbille
--%>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>HelloComplex</title>
    </head>
    <body>
        <h1>Hello ${complexObject.name}!</h1>
		<p>You are ${complexObject.age} years old</p>
    </body>
</html>
