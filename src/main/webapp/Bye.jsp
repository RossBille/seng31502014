<%-- 
    Document   : Bye
    Created on : Apr 10, 2014, 11:53:28 PM
    Author     : rossbille
--%>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bye</title>
    </head>
    <body>
        <h1>Bye ${name}!</h1>
    </body>
</html>
