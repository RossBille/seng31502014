package au.edu.newcastle.seng31502014.airpub;

import au.edu.newcastle.seng31502014.airpub.model.ComplexObject;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;

/**
 *
 * @author rossbille
 */
public class ComplexObjects
{
	
	private ComplexObject complexObject;

	/**
	 * Get the value of complexObject
	 *
	 * @return the value of complexObject
	 */
	public ComplexObject getComplexObject()
	{
		return complexObject;
	}

	/**
	 * Set the value of complexObject
	 *
	 * @param complexObject new value of complexObject
	 */
	public void setComplexObject(ComplexObject complexObject)
	{
		this.complexObject = complexObject;
	}
	public String execute(){
		if(this.complexObject.getAge() > 100){
			return "fail";
		}
		if(this.complexObject.getAge() > 20){
			return SUCCESS;
		}
		return ERROR;
	}
}
