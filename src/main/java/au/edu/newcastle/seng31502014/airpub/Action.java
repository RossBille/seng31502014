package au.edu.newcastle.seng31502014.airpub;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;

/**
 *
 * @author rossbille
 */
public class Action
{
	public String execute(){
		return SUCCESS;
	}
	public String error(){
		return ERROR;
	}
	public String success(){
		return SUCCESS;
	}
}
