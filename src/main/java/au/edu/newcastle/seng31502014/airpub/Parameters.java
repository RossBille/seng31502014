package au.edu.newcastle.seng31502014.airpub;

import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;

/**
 *
 * @author rossbille
 */
public class Parameters extends ActionSupport
{
	
	private String name;

	/**
	 * Get the value of name
	 *
	 * @return the value of name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Set the value of name
	 *
	 * @param name new value of name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	public String execute(){
		if(name.equals("ross")){
			return SUCCESS;
		}
		return ERROR;
	}
	
}
