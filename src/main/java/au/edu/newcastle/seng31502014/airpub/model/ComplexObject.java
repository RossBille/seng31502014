package au.edu.newcastle.seng31502014.airpub.model;

/**
 *
 * @author rossbille
 */
public class ComplexObject
{
	
	private String name;

	/**
	 * Get the value of name
	 *
	 * @return the value of name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Set the value of name
	 *
	 * @param name new value of name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	private int age;

	/**
	 * Get the value of age
	 *
	 * @return the value of age
	 */
	public int getAge()
	{
		return age;
	}

	/**
	 * Set the value of age
	 *
	 * @param age new value of age
	 */
	public void setAge(int age)
	{
		this.age = age;
	}

}
